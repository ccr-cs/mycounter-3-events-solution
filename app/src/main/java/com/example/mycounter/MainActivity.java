package com.example.mycounter;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    //integer count that can be incremented and reset
    private int mCount = 0;
    // TextView that will be used to change the displayed count
    private TextView mShowCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //find the View that has ID of the TextView that shows the count.
        mShowCount = findViewById(R.id.textView_count);
    }

    //click handler method that adds one to the count
    public void countUp(View view) {
        mCount++; //add 1 to mCount
        if (mShowCount != null)
            mShowCount.setText(String.valueOf(mCount));
    }

    //click handler method that resets the count
    public void reset(View view) {
        mCount = 0;
        if (mShowCount != null)
            mShowCount.setText(String.valueOf(mCount));
    }
}